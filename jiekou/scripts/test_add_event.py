
import requests
import allure

import pytest
import random
@allure.title("添加发布会接口")
def test_add_event():
    num = random.randint(10,200)

    url = 'http://127.0.0.1:8000/api/add_event/'
    body = {"eid":num,
            "name":"发布会{}".format(num),
            "address":"天府新谷",
            "limit":15,
            "start_time":"2022-10-15",
            "status":1}
    res = requests.post(url=url,data=body)
    print(body)
    print(res.text)
    print(res.status_code)
if __name__ == '__main__':
    pytest.main(['-vs','test_add_event.py'])

